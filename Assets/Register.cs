using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Register : MonoBehaviour
{
    public InputField nameField, emailField, passwordField;
    public Text InfoDisplay;

    public Button submitButton;


    public void CallRegister()
    {
        StartCoroutine(RegisterOn());
    }

    IEnumerator RegisterOn()
    {
        WWWForm form = new WWWForm();
        form.AddField("name", nameField.text.ToLower().Trim());
        form.AddField("email", emailField.text.ToLower().Trim());
        form.AddField("password", passwordField.text.Trim());
        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/register.php", form))
        {
            www.downloadHandler = new DownloadHandlerBuffer();
            yield return www.SendWebRequest();


            if (www.result != UnityWebRequest.Result.Success)//www.result != UnityWebRequest.Result.Success
            {
                Debug.Log(www.error);
            }
            else
            {
                string responseText = www.downloadHandler.text;
                if (responseText == "1")
                {
                    InfoDisplay.text = "Brak po�acznia z baz� danych";
                    
                }
                else if (responseText == "2")
                {
                    InfoDisplay.text = "Taki email ju� istnie w bazie";
                }
                else if (responseText == "3")
                {

                    //InfoDisplay.text = "sukces";
                    PlayerPrefs.SetString("name", nameField.text);
                    PlayerPrefs.SetString("email", emailField.text);
                    SceneManager.LoadSceneAsync("MenuRegions");
                }
                else if (responseText == "4")
                {
                    InfoDisplay.text = "Wstawienie  u�ytkownika do bazy nie powiod�o si�";
                }
                else if (responseText == "5")
                {
                    InfoDisplay.text = "Nie poprawny format emaila";
                }
                
                else
                {
                    InfoDisplay.text = ("Co� posz�o nie tak - spr�buj p�niej");
                    
                }
            }
        }
    }
    public void VerifyInputs()
    {
        submitButton.interactable = (nameField.text.Length > 2 && emailField.text.Length > 5 && passwordField.text.Length >= 8);
        
        //Debug.Log("Dane zapisano " + PlayerPrefs.GetString("email"));
    }
}
