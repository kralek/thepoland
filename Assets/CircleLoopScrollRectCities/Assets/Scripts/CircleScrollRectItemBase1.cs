﻿using System;
using UnityEngine;
using UnityEngine.UI;

public abstract class CircleScrollRectItemBase1 : MonoBehaviour
{
    [NonSerialized]
    public CircleScrollRectItemBase1 prevItem1;
    [NonSerialized]
    public CircleScrollRectItemBase1 nextItem1;
    [NonSerialized]
    public int currPosIndex1; //index in position list
    [NonSerialized]
    public int contentListIndex1; //index in content list

    private int itemListIndex1;   //index in ui item list     
    private int contentListLength1; //content list length

    //Refresh item data
    public abstract void RefreshItemContent(object data);
    //Get content list item
    public abstract object GetContentListItem(int index);
    //Get content list length
    public abstract int GetContentListLength();

    /// <summary>
    /// Init config
    /// </summary>
    /// <param name="id"></param>
    /// <param name="nItem"></param>
    /// <param name="pItem"></param>
    public void SetItemConfig(int id, CircleScrollRectItemBase1 nItem, CircleScrollRectItemBase1 pItem)
    {
        this.itemListIndex1 = id;
        this.prevItem1 = pItem;
        this.nextItem1 = nItem;
    }

    /// <summary>
    /// Calculate content list index
    /// </summary>
    /// <param name="itemListLen"></param>
    public void RefreshContentListLength(int itemListLen)
    {
        contentListLength1 = GetContentListLength();

        //fixed：advoid endless loop
        if (contentListLength1 <= 0)
        {
            contentListIndex1 = 0;
            return;
        }

        //start from center
        int centerIndex = itemListLen / 2;
        if (itemListIndex1 == centerIndex)
        {
            contentListIndex1 = 0;
        }
        else if (itemListIndex1 < centerIndex)
        {
            contentListIndex1 = contentListLength1 - (centerIndex - itemListIndex1);
        }
        else
        {
            contentListIndex1 = itemListIndex1 - centerIndex;
        }
        //circular
        while (contentListIndex1 < 0)
        {
            contentListIndex1 += contentListLength1;
        }
        //mod
        contentListIndex1 = contentListIndex1 % contentListLength1;

        UpdateToCurrentContent();
    }

    /// <summary>
    /// Refresh content to current one
    /// </summary>
    public void UpdateToCurrentContent()
    {
        RefreshItemContent(GetContentListItem(contentListIndex1));
    }

    /// <summary>
    /// Refresh content to backward direction
    /// </summary>
    /// <param name="shiftStep">shift step</param>
    public void UpdateToPrevContent(int shiftStep = 1)
    {
        contentListIndex1 = nextItem1.contentListIndex1 - shiftStep;
        contentListIndex1 = (contentListIndex1 < 0) ? contentListLength1 - 1 : contentListIndex1;
        RefreshItemContent(GetContentListItem(contentListIndex1));
    }

    /// <summary>
    /// Refresh content to forward direction
    /// </summary>
    /// <param name="shiftStep">shift step</param>
    public void UpdateToNextContent(int shiftStep = 1)
    {
        contentListIndex1 = prevItem1.contentListIndex1 + shiftStep;
        contentListIndex1 = (contentListIndex1 == contentListLength1) ? 0 : contentListIndex1;
        RefreshItemContent(GetContentListItem(contentListIndex1));
    }
}
