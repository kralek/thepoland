﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CircleItemContent1
{
    public string name1;
    public Sprite icon1;
}

public class CircleTestItem1 : CircleScrollRectItemBase1
{
    public Text nameTxt1;
    public Image iconImg1;

    public override void RefreshItemContent(object data)
    {
        var contentItem = data as CircleItemContent1;
        if(null != contentItem)
        {
            if(null != nameTxt1)
            {
                nameTxt1.text = contentItem.name1;
            }
            if(null != iconImg1)
            {
                iconImg1.sprite = contentItem.icon1;
            }          
        }
        
    }

    public override object GetContentListItem(int index)
    {
        return TestMono1.ins.GetItem(index);
    }

    public override int GetContentListLength()
    {
        return TestMono1.ins.GetItemLength();
    }

}
