﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;


[SerializeField]
public class CircleScrollRect1 : UIBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    //UI Item List
    //[SerializeField] //Change to serializefield if you want to add item from inspector
    [NonSerialized]
    public CircleScrollRectItemBase1[] listItems1;
    //Slide Direction
    [SerializeField]
    public bool horizontal1 = false;
    //Sliding threshold
    [NonSerialized]
    public float oneShiftThreshold1 = 90.0f;
    //UI Item Positin List
    private Vector3[] itemPostions1;
    //Start position of draging
    private Vector2 dragStartPostion1;
    //if the list need to adjust after draging
    private bool needAdjust1 = false;
    //Drag to dest position event
    public class ReachDestinationEvent : UnityEvent<int> { }
    //Event
    private ReachDestinationEvent onDragToTargetPosition1 = new ReachDestinationEvent();
    public ReachDestinationEvent OnDragToTargetPosition1
    {
        get { return onDragToTargetPosition1; }
        set { onDragToTargetPosition1 = value; }
    }
    //Target position index
    public int targetPositionIndex1 = -1;
    //Target content list index
    private int targetContentListIndex1 = -1;
    /// <summary>
    /// Comparision of position y
    /// </summary>
    private Comparison<CircleScrollRectItemBase1> ComparisionY1 =
        delegate (CircleScrollRectItemBase1 itemA, CircleScrollRectItemBase1 itemB)
        {
            if (itemA.transform.localPosition.y == itemB.transform.localPosition.y) return 0;
            return (itemA.transform.localPosition.y > itemB.transform.localPosition.y) ? -1 : 1;
        };

    /// <summary>
    ///Comparision of position x
    /// </summary>
    private Comparison<CircleScrollRectItemBase1> ComparisionX1 =
        delegate (CircleScrollRectItemBase1 itemA, CircleScrollRectItemBase1 itemB)
        {
            if (itemA.transform.localPosition.x == itemB.transform.localPosition.x) return 0;
            return (itemA.transform.localPosition.x > itemB.transform.localPosition.x) ? -1 : 1;
        };

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();
        //Get child items
        listItems1 = new CircleScrollRectItemBase1[gameObject.transform.childCount];
        int index = 0;
        foreach (Transform trans in gameObject.transform)
        {
            //Debug.Log("child" + trans.name);
            CircleScrollRectItemBase1 item = trans.GetComponent<CircleScrollRectItemBase1>();
            listItems1[index++] = item;
        }
        //Check
        if (null == listItems1 || listItems1.Length == 0)
        {
            return;
        }
        //Sort UI item list by postion
        if (horizontal1)
        {
            //ascending order
            Array.Sort(listItems1, ComparisionX1);
        }
        else
        {
            // descending order
            Array.Sort(listItems1, ComparisionY1);
        }
        
        //element relationship and positions
        itemPostions1 = new Vector3[listItems1.Length];
        for (int i = 0; i < listItems1.Length; ++i)
        {
            listItems1[i].SetItemConfig(i, //set id
                                       listItems1[(i + 1) % listItems1.Length], //next item
                                       listItems1[(i - 1 + listItems1.Length) % listItems1.Length]); //previous item

            itemPostions1[i] = new Vector3(listItems1[i].transform.localPosition.x,
                                            listItems1[i].transform.localPosition.y,
                                              listItems1[i].transform.localPosition.z);
            listItems1[i].currPosIndex1 = i;
        }
        RefreshContentListLength();
        TriggerDragToTargetPostionEvent();
        targetContentListIndex1 = -1;
        if (targetPositionIndex1 < 0)
        {
            targetPositionIndex1 = listItems1.Length / 2;
        }
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        listItems1 = null;
    }

    private void Update()
    {

    }
    
    /// <summary>
    /// Begin Dragging
    /// </summary>
    /// <param name="eventData"></param>
    public virtual void OnBeginDrag(PointerEventData eventData)
    {
        needAdjust1 = false;
        dragStartPostion1 = eventData.position;
    }

    /// <summary>
    /// Update postion when dragging
    /// </summary>
    /// <param name="eventData"></param>
    public virtual void OnDrag(PointerEventData eventData)
    {
        if (horizontal1)
        {
            if(ShiftListHorizontal(eventData.position - dragStartPostion1))
            {
                dragStartPostion1 = eventData.position; //Update the start draging postion
            }
        }
        else
        {
            if (ShiftListVertical(eventData.position - dragStartPostion1))
            {
                dragStartPostion1 = eventData.position; //Update the start draging postion
            }
        }

    }

    /// <summary>
    /// End Drag
    /// </summary>
    /// <param name="eventData"></param>
    public virtual void OnEndDrag(PointerEventData eventData)
    {
        if(needAdjust1)
        {
            if (horizontal1)
            {
                AdjustLocationX();
            }
            else
            {
                AdjustLocationY();
            }       
        }
        TriggerDragToTargetPostionEvent();
    }

    /// <summary>
    /// Refresh When the conent list changes
    /// </summary>
    public void RefreshContentListLength()
    {
        for (int i = 0; i < listItems1.Length; ++i)
        {
            listItems1[i].RefreshContentListLength(listItems1.Length);
        }
    }

    /// <summary>
    /// Shift the list when dragging vertical
    /// </summary>
    /// <param name="delta"></param>
    /// <returns>need update initial dragging postion</returns>
    public bool ShiftListVertical(Vector2 delta)
    {
        if (null == listItems1 || listItems1.Length < 2 || delta.y == 0)
        {
            return false;
        }
        needAdjust1 = true;
        bool downSlide = (delta.y < 0); 
        for (int i = 0; i < listItems1.Length; ++i)
        {
            int curIndex = listItems1[i].currPosIndex1;
            int targetIndex = downSlide ? curIndex + 1 : curIndex - 1;

            if (targetIndex >= 0 && targetIndex < listItems1.Length)
            {
                //Lerp ratio
                float ratio = Mathf.Abs(delta.y) / oneShiftThreshold1;
                //Lerp
                if (ratio <= 1)
                {
                    listItems1[i].transform.localPosition = Vector3.Lerp(listItems1[i].transform.localPosition,
                                                                            itemPostions1[targetIndex], ratio);
                }
                //If reach the target postion
                if (ratio >= 1 || Mathf.Abs(listItems1[i].transform.localPosition.y - itemPostions1[targetIndex].y) <= 1.0f)
                {                       
                    ShiftOneTime(!downSlide);
                    return true;
                }
            }
        }          
        
        return false;
    }

    /// <summary>
    /// Shift the list when dragging horizontal
    /// </summary>
    /// <param name="delta"></param>
    /// <returns>need update initial dragging postion</returns>
    public bool ShiftListHorizontal(Vector2 delta)
    {
        if (null == listItems1 || listItems1.Length < 2 || delta.x == 0)
        {
            return false;
        }
        needAdjust1 = true;
        bool leftSlide = (delta.x < 0);
        for (int i = 0; i < listItems1.Length; ++i)
        {
            int curIndex = listItems1[i].currPosIndex1;
            int targetIndex = leftSlide ? curIndex + 1 : curIndex - 1;

            if (targetIndex >= 0 && targetIndex < listItems1.Length)
            {              
                float ratio = Mathf.Abs(delta.x) / oneShiftThreshold1;

                if (ratio <= 1)
                {
                    listItems1[i].transform.localPosition = Vector3.Lerp(listItems1[i].transform.localPosition,
                                                                            itemPostions1[targetIndex], ratio);
                }

                if (ratio >= 1 || Mathf.Abs(listItems1[i].transform.localPosition.x - itemPostions1[targetIndex].x) <= 1.0f)
                {
                    ShiftOneTime(!leftSlide);
                    return true;
                }
            }
        }

        return false;
    }

    /// <summary>
    /// Do one circular shift one time
    /// </summary>
    /// <param name="reverse">reverse shift</param>
    private void ShiftOneTime(bool reverse, int shiftStep = 1)
    {
        needAdjust1 = false; 
        for (int i = 0; i < listItems1.Length; ++i)
        {
            int curIndex = listItems1[i].currPosIndex1;
            if (reverse) 
            {
                listItems1[i].transform.localPosition = itemPostions1[(curIndex - shiftStep + listItems1.Length) % listItems1.Length];
                listItems1[i].currPosIndex1 = (curIndex - shiftStep + listItems1.Length) % listItems1.Length;
                if(curIndex - shiftStep < 0)
                {
                    listItems1[i].UpdateToNextContent();
                }         
            }
            else
            {
                listItems1[i].transform.localPosition = itemPostions1[(curIndex + shiftStep) % itemPostions1.Length];
                listItems1[i].currPosIndex1 = (curIndex + shiftStep) % itemPostions1.Length;
                if(curIndex + shiftStep >= itemPostions1.Length)
                {
                    
                    listItems1[i].UpdateToPrevContent();
                }
            }

        }
    }
    private void SlideItemPosition(bool reverse, int closestIndex, int targetIndex)
    {
        for (int i = 0; i < listItems1.Length; ++i)
        {
            int curIndex = listItems1[i + closestIndex].currPosIndex1;
            if(i == 0)
            {

                continue;
            }
            if (reverse)
            {
                listItems1[i].transform.localPosition = itemPostions1[(curIndex - 1 + listItems1.Length) % listItems1.Length];
                listItems1[i].currPosIndex1 = (curIndex - 1 + listItems1.Length) % listItems1.Length;
                if (curIndex - 1 < 0)
                {
                    listItems1[i].UpdateToNextContent();
                }
            }
            else
            {
                listItems1[i].transform.localPosition = itemPostions1[(curIndex + 1) % itemPostions1.Length];
                listItems1[i].currPosIndex1 = (curIndex + 1) % itemPostions1.Length;
                if (curIndex + 1 >= itemPostions1.Length)
                {

                    listItems1[i].UpdateToPrevContent();
                }
            }

        }
    }
    /// <summary>
    /// Adjust position y
    /// </summary>
    public void AdjustLocationY()
    {
        int shiftStep = 1;
        int halfPosIndex = itemPostions1.Length / 2;
        //Attach to target position
        if(targetPositionIndex1 < itemPostions1.Length)
        {
            halfPosIndex = targetPositionIndex1;
        }
        //find the item closest to target poistion
        bool reverse = false;
        float disMin = float.MaxValue;
        for (int i = 0; i < listItems1.Length; ++i)
        {
            float dis = listItems1[i].transform.localPosition.y - itemPostions1[halfPosIndex].y;
            if (Mathf.Abs(dis) < disMin)
            {
                disMin = Mathf.Abs(dis);
                reverse = (dis < 0) ? true : false; //upside is the reverse direction
                shiftStep = Math.Abs(listItems1[i].currPosIndex1 - halfPosIndex);
            }
        }
        //var item = (CircleItemContent)listItems[targetIndex].GetContentListItem(listItems[targetIndex].contentListIndex);
        //Debug.LogError("Direction: " + reverse.ToString() + "; step: " + shiftStep + "; Target: " + item.name);
        ShiftOneTime(reverse, shiftStep);        
    }
    /// <summary>
    /// Adjust position x
    /// </summary>
    public void AdjustLocationX()
    {
        int shiftStep = 1;
        int halfPosIndex = itemPostions1.Length / 2;
        //Attach to target position
        if (targetPositionIndex1 < itemPostions1.Length)
        {
            halfPosIndex = targetPositionIndex1;
        }
        //find the item closest to target poistion
        bool reverse = false;
        float disMin = float.MaxValue;
        for (int i = 0; i < listItems1.Length; ++i)
        {
            float dis = listItems1[i].transform.localPosition.x - itemPostions1[halfPosIndex].x;
            if (Mathf.Abs(dis) < disMin)
            {
                disMin = Mathf.Abs(dis);
                reverse = (dis < 0) ? true : false; //upside is the reverse direction
                shiftStep = Math.Abs(listItems1[i].currPosIndex1 - halfPosIndex);
            }
        }
        ShiftOneTime(reverse, shiftStep);
    }

    /// <summary>
    /// Trigger event while dragging to target position
    /// </summary>
    private void TriggerDragToTargetPostionEvent()
    {
        if(targetPositionIndex1 < listItems1.Length)
        {
            int targetIndex = 0;
            for(int i = 0; i < listItems1.Length; ++i)
            {
                if(listItems1[i].currPosIndex1 == targetPositionIndex1)
                {
                    targetIndex = listItems1[i].contentListIndex1;
                    break;
                }
            }
            if(targetContentListIndex1 != targetIndex)
            {
                targetContentListIndex1 = targetIndex;
                //Debug.Log(targetIndex);
                if (null != onDragToTargetPosition1)
                {                    
                    onDragToTargetPosition1.Invoke(targetContentListIndex1);
                }
            }
        }
    }
}
