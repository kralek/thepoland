﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestMono1 : MonoBehaviour {

    public CircleScrollRect1 scrollRect1;
    public Button btn1;
    public Text descText1;

    [SerializeField]
    public Sprite[] contentSpritePool1;

    public static TestMono1 ins;

    List<CircleItemContent1> content1 = new List<CircleItemContent1>();
    string[] City = new string[] { "Szemud i okolice", "Łęczyce i okolice", "Luzino i okolice", "Linia i okolice", "Gniewino i okolice", "Choczewo i okolice", "Władysławowo i okolice", "Hel i okolice", "Gdynia", "Sopot", "Gdańsk", "Kosakowo i okolice", "Słupsk i okolice", "Puck i okolice", "Krokowa i okolice", "Wejherowo i okolice" };

    // Use this for initialization
    private void Awake () {
        ins = this;
        RandomizeContentList();

        if (null != btn1)
        {
            btn1.onClick.AddListener(ChangeContentListLength);
        }
        if(null != scrollRect1)
        {
            scrollRect1.OnDragToTargetPosition1.AddListener(OnDragToTargetPosition);
        }

    }

    private void OnDestroy()
    {
        if (null != btn1)
        {
            btn1.onClick.RemoveListener(ChangeContentListLength);
        }
        if (null != scrollRect1)
        {
            scrollRect1.OnDragToTargetPosition1.RemoveListener(OnDragToTargetPosition);
        }
    }

    public CircleItemContent1 GetItem(int index)
    {
        return content1[index];
    }
    public int GetItemLength()
    {
        return content1.Count;
    }

    public void OnDragToTargetPosition(int targetContentListIndex)
    {
        if(null != descText1)
        {
            //descText1.text = "The selected content list item: \n {index:name} = {" + targetContentListIndex.ToString() + ": " + content1[targetContentListIndex].name1.ToString() + "}";
            descText1.text = City[targetContentListIndex].ToString();
        }
    }

    private void RandomizeContentList()
    {
        int len = UnityEngine.Random.Range(16, contentSpritePool1.Length);
        content1.Clear();
        for (int i = 0; i < len; ++i)
        {
            CircleItemContent1 item = new CircleItemContent1();
            item.icon1 = contentSpritePool1[i];
            item.name1 = "item" + i.ToString();//contentNamePool[i];
            content1.Add(item);
        }
        if (null != descText1)
        {
            descText1.text = "The length of content list changed: " + len.ToString();
        }
    }

    public void ChangeContentListLength()
    {
        RandomizeContentList();

        //When you change content list, you should call .RefreshContentListLength()
        if (null != scrollRect1)
        {
            scrollRect1.RefreshContentListLength();
        }
    }

}
