using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Android;


public class CheckGPS : MonoBehaviour
{
    [SerializeField] Text DisplayErrorGPS;
    [SerializeField] GameObject PanelErrorGPS;
    public bool isUpdating;
    public float GPSStatusValue, LatitudeValue, DGGVaule, WnPMVaule, DPVaule, TimeStampVaule;

    void Update()
    {
        
        if (!isUpdating)
        {
            StartCoroutine(GetLocation());
            isUpdating = !isUpdating;
        }
    }
    IEnumerator GetLocation()
    {
        if (!Permission.HasUserAuthorizedPermission(Permission.FineLocation))
        {
            Permission.RequestUserPermission(Permission.FineLocation);
            Permission.RequestUserPermission(Permission.CoarseLocation);
        }
        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
            yield return new WaitForSeconds(1);

        // Start service before querying location
        Input.location.Start();

        // Wait until service initializes
        int maxWait = 2;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            isUpdating = !isUpdating;
            CheckEnableGPS(true);
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            isUpdating = !isUpdating;
            CheckEnableGPS(true);
            yield break;
        }
        else
        {
            //gpsOut.text = LatitudeGPS.ToString();
            //LatitudeGPS = Input.location.lastData.latitude;
            //GPSStatusValue.text = "do zrobienia";
            LatitudeValue = (Input.location.lastData.latitude);
            DGGVaule = Input.location.lastData.longitude;
            WnPMVaule = (Input.location.lastData.altitude);
            DPVaule = Input.location.lastData.horizontalAccuracy;
            //TimeStampVaule = Input.location.lastData.timestamp;
            CheckEnableGPS(false);

            //"GPS w��czony"+ 
            //("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
        }

        // Stop service if there is no need to query location updates continuously
        isUpdating = !isUpdating;
        Input.location.Stop();
    }

    void CheckEnableGPS(bool Enabled)
    {
        if (Enabled)
        {
            PanelErrorGPS.SetActive(Enabled);
            DisplayErrorGPS.text = "W��cz GPS";
        }
        else
        {
            PanelErrorGPS.SetActive(Enabled);
            //DisplayErrorGPS.text = "";
        }
        

    }

}
/*using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Android;

public class GPS : MonoBehaviour
{
    
    [SerializeField] Text GPSStatusValue, LatitudeValue, DGGVaule, WnPMVaule, DPVaule, TimeStampVaule;
    [SerializeField] GameObject Panel;
    float LatitudeGPS;
    public Text gpsOut;
    public bool isUpdating;

    private void Update()
    {
        if (!isUpdating)
        {
            StartCoroutine(GetLocation());
            isUpdating = !isUpdating;
        }
    }
    IEnumerator GetLocation()
    {
        if (!Permission.HasUserAuthorizedPermission(Permission.FineLocation))
        {
            Permission.RequestUserPermission(Permission.FineLocation);
            Permission.RequestUserPermission(Permission.CoarseLocation);
        }
        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
            yield return new WaitForSeconds(1);

        // Start service before querying location
        Input.location.Start();

        // Wait until service initializes
        int maxWait = 2;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            gpsOut.text = "Timed out";
            print("Timed out");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            gpsOut.text = "Unable to determine device location";
            print("Unable to determine device location");
            yield break;
        }
        else
        {
            LatitudeGPS = Input.location.lastData.latitude;
            LatitudeValue.text = (Input.location.lastData.latitude).ToString();
            DGGVaule.text = Input.location.lastData.longitude.ToString();
            WnPMVaule.text = (Input.location.lastData.altitude).ToString();
            DPVaule.text = Input.location.lastData.horizontalAccuracy.ToString();
            TimeStampVaule.text = Input.location.lastData.timestamp.ToString();
            CheckPos();
        }

        // Stop service if there is no need to query location updates continuously
        isUpdating = !isUpdating;
        Input.location.Stop();
    }
    public void GetLocalization()
    {
        GetLocation();
    }
    public void ResetData(string cher)
    {
        gpsOut.text = cher;
        GPSStatusValue.text = cher;
        LatitudeValue.text = cher;
        DGGVaule.text = cher;
        WnPMVaule.text = cher;
        DPVaule.text = cher;
        TimeStampVaule.text = cher;
    }
    public void DisplayPanel(bool Enabled)
    {
        Panel.SetActive(Enabled);
    }

    void SetCheckPosition(bool EnabledOne,bool EnabledTwo)
    {
        GameObject.FindGameObjectWithTag("StartSound").GetComponent<Button>().interactable = EnabledOne;
        GameObject.FindGameObjectWithTag("StopSound").GetComponent<Button>().interactable = EnabledTwo;
    }

    public void CheckPos()
    {
        CheckPosition(54559913, 54560074, 18491252, 18491640, 0);
    }
     void CheckPosition(int MinX, int MaxX, int MinY, int MaxY, int Lati)
    {
        int scala = 1000000;
        if ((Input.location.lastData.latitude * scala > MinX) &&
            (Input.location.lastData.latitude * scala < MaxX))
        {
            if (((Input.location.lastData.longitude * scala > MinY) &&
            (Input.location.lastData.longitude * scala < MaxY)))
            {
                if (Input.location.lastData.altitude >= Lati)
                {
                    gpsOut.text = ((int)(Input.location.lastData.latitude * scala)).ToString();
                    GPSStatusValue.text = " Sukcess";
                    GPSStatusValue.color = Color.green;
                    //GetCheckPosition(true, true);
                }
                else
                {
                    gpsOut.text = ((int)(Input.location.lastData.latitude * scala)).ToString();
                    GPSStatusValue.text = " wysoko�� nie poprawna";
                    GPSStatusValue.color = Color.blue;
                }
            }
            else
            {
                gpsOut.text = ((int)(Input.location.lastData.latitude * scala)).ToString();
                GPSStatusValue.text = "B��dna szer.";
                GPSStatusValue.color = Color.blue;
            }
        }
        else
        {
            gpsOut.text = ((int)(Input.location.lastData.latitude * scala)).ToString();
            GPSStatusValue.text = " B��dna d�.";
            GPSStatusValue.color = Color.red;
            SetCheckPosition(true, true);


        }
    }
}
*/
