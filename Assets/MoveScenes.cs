using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveScenes : MonoBehaviour
{
    public void MoveGoScenes(string Name)
    {
        SceneManager.LoadScene(Name);
    }
    public void QuitApp()
    {
        Application.Quit();
    }
    public void Reset()
    {
        PlayerPrefs.DeleteKey("name");
        SceneManager.LoadScene("FristMenu");
    }
}
