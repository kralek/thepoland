using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class Search : MonoBehaviour
{
    public InputField SearchCity;
    public GameObject PanelNoCity;
    public Button SearchBtm;

    public void SearchCityGo()
    {
        string[] Cities = { "GDYNIA", "KOSAKOWO",};
        string nameCity = SearchCity.text.ToUpper().Trim();

        int Index = Array.IndexOf(Cities, nameCity);

        if(nameCity != "")
        {
            if (Index > -1)
            {
                if (nameCity == Cities[Index])
                {
                    SceneManager.LoadScene("Menu" + Cities[Index]);
                }
            }
            else
            {
                DisabledPanelCity(true);
            }
        }
    }
    public void DisabledPanelCity(bool Enabled)
    {
        PanelNoCity.SetActive(Enabled);
    }
}
