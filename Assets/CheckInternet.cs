using UnityEngine;
using UnityEngine.UI;


public class CheckInternet : MonoBehaviour
{
    [SerializeField] Text m_ReachabilityText;
    [SerializeField] GameObject PanelErrorInternet;
   


    void Update()
    {

        //Output the network reachability to the console window
        //Check if the device cannot reach the internet
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            //Change the Text
            EnabledPanelError(true);
            
        }
        /*Check if the device can reach the internet via a carrier data network
        else if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork)
        {
            m_ReachabilityText.text = "";
            //m_ReachabilityText.text = "Internet W��czony LTE.";
        }
        //Check if the device can reach the internet via a LAN
        else if (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
        {
            m_ReachabilityText.text = "";
            //m_ReachabilityText.text = "Internet W��czony WIFI";
        }*/
        else
        {
            EnabledPanelError(false);
        }
    }
    // Check connect Internet
    void EnabledPanelError(bool Enabled)
    {
        if (Enabled)
        {
            PanelErrorInternet.SetActive(Enabled);
            m_ReachabilityText.text = "W��cz Internet";
        }
        else
        {
            PanelErrorInternet.SetActive(Enabled);
            //m_ReachabilityText.text = "Dzia�a";
        }
    }
    
}